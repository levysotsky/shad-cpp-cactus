#include <benchmark/benchmark.h>

#include <functional>

#include "context.h"

using namespace cactus;

struct FnContextEntry : public IContextEntry {
    template <class Fn>
    FnContextEntry(Fn fn) : fn(fn) {
    }

    std::function<SavedContext*()> fn;

    virtual SavedContext* Run() override {
        return fn();
    }
};

static void ContextSwitch(benchmark::State& state) {
    std::vector<char> stack(4096);
    SavedContext fiber_context(stack.data(), stack.size());
    SavedContext main_context;

    bool exit = false;

    FnContextEntry run = [&] {
        while (!exit) {
            main_context.Activate(&fiber_context);
        }

        return &main_context;
    };
    fiber_context.entry = &run;

    for (auto _ : state) {
        fiber_context.Activate(&main_context);
    }

    exit = true;
    fiber_context.Activate(&main_context);

    state.SetItemsProcessed(2 * state.iterations());
}

BENCHMARK(ContextSwitch);

BENCHMARK_MAIN();