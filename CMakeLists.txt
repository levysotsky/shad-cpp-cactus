project(cactus)

cmake_minimum_required(VERSION 3.9)

# ==============================================================================

set(CMAKE_CXX_STANDARD                 17)
set(CMAKE_MODULE_PATH                  "${CMAKE_SOURCE_DIR}/cmake;${CMAKE_BINARY_DIR}")
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY     "${CMAKE_BINARY_DIR}")
set(CMAKE_EXPORT_COMPILE_COMMANDS      ON)
set(CMAKE_CXX_FLAGS_ASAN "-g -fsanitize=address,undefined -fno-sanitize-recover=all"
  CACHE STRING "Compiler flags in asan build"
  FORCE)

if (NOT UNIX OR APPLE)
  message(FATAL_ERROR "Only linux build is supported. See instructions for remote toolchain setup.")
endif()

# ==============================================================================

include(cmake/conan.cmake)

conan_check(REQUIRED)

conan_cmake_run(CONANFILE conanfile.txt
  BASIC_SETUP CMAKE_TARGETS
  BUILD_TYPE Release
  BUILD missing)

# ==============================================================================

add_compile_options(-Wall)
include_directories(.)

find_package(Benchmark REQUIRED)
find_package(Catch REQUIRED)
find_package(Protobuf REQUIRED)
find_package(ProtoLibrary REQUIRED)

enable_testing()

# ==============================================================================

add_subdirectory(cactus)

add_subdirectory(examples)

add_subdirectory(portscan)
add_subdirectory(portknock)
add_subdirectory(socks4)
add_subdirectory(fanout)

function(add_subdirectory_if_exists name)
  if(IS_DIRECTORY "${CMAKE_CURRENT_SOURCE_DIR}/${name}")
    add_subdirectory(${name})
  endif()
endfunction()

add_subdirectory_if_exists(balancer)
add_subdirectory_if_exists(flagd)
add_subdirectory_if_exists(redis)
add_subdirectory_if_exists(socksd)
