#include <catch2/catch.hpp>

#include <thread>
#include <mutex>
#include <utility>

#include <redis/client.h>

#include <cactus/io/reader.h>
#include <cactus/net/net.h>
#include <cactus/core/fiber.h>
#include <cactus/test.h>

using namespace cactus;
using namespace redis;

static const auto kServerAddr = folly::SocketAddress(folly::IPAddressV4("127.0.0.1"), 0);

class MockServer {
public:
    MockServer(const folly::SocketAddress& address)
        : listener_(cactus::ListenTCP(address)) {
    }

    const folly::SocketAddress& GetAddress() const {
        return listener_->Address();
    }

    void RegisterResponses(const std::vector<std::pair<std::string, std::string>>& script) {
        CHECK(!fiber_);
        script_.insert(script_.end(), script.begin(), script.end());
    }

    void Run() {
        auto conn = listener_->Accept();
        while (current_index_ < script_.size()) {
            const auto& [expected_req, rsp] = script_[current_index_];

            std::vector<char> buf(expected_req.size());
            conn->ReadFull(View(buf));

            REQUIRE(expected_req == std::string_view(buf.data(), buf.size()));
            conn->Write(View(rsp));
            current_index_++;
        }
    }

    void Start() {
        CHECK(!fiber_);
        CHECK(!script_.empty());

        fiber_ = std::make_unique<Fiber>([this] {
            Run();
        });
    }

    void Finish() {
        REQUIRE(current_index_ == script_.size());
    }

private:
    std::unique_ptr<cactus::IListener> listener_;
    std::vector<std::pair<std::string, std::string>> script_;
    size_t current_index_ = 0;

    std::unique_ptr<cactus::Fiber> fiber_;
};

std::pair<std::unique_ptr<Client>, std::unique_ptr<MockServer>> CreateClientAndServer(
    const std::vector<std::pair<std::string, std::string>>& script)
{
    auto server = std::make_unique<MockServer>(kServerAddr);
    server->RegisterResponses(script);
    server->Start();
    return std::make_pair(std::make_unique<Client>(server->GetAddress()), std::move(server));
}

FIBER_TEST_CASE("Client") {
    SECTION("Get") {
        auto [client, server] = CreateClientAndServer(
            {
                {
                    "*2\r\n" "$3\r\nGET\r\n" "$3\r\nkey\r\n",
                    "$5\r\nvalue\r\n"
                },
                {
                    "*2\r\n" "$3\r\nGET\r\n" "$7\r\nnot-key\r\n",
                    "$-1\r\n"
                }
            });
        REQUIRE(client->Get("key") == "value");
        REQUIRE(client->Get("not-key") == std::nullopt);

        server->Finish();
    }

    SECTION("Set") {
        auto [client, server] = CreateClientAndServer(
            {
                {
                    "*3\r\n" "$3\r\nSET\r\n" "$3\r\nkey\r\n" "$5\r\nvalue\r\n",
                    "$2\r\nOK\r\n"
                },
                {
                    "*3\r\n" "$3\r\nSET\r\n" "$4\r\nkey1\r\n" "$6\r\nvalue1\r\n",
                    "-ERR Some error\r\n"
                }
            });
        REQUIRE_NOTHROW(client->Set("key", "value"));
        REQUIRE_THROWS_AS(client->Set("key1", "value1"), RedisError);

        server->Finish();
    }

    SECTION("Increment") {
        auto [client, server] = CreateClientAndServer(
            {
                {
                    "*2\r\n" "$4\r\nINCR\r\n" "$3\r\nkey\r\n",
                    ":1\r\n"
                },
                {
                    "*2\r\n" "$4\r\nINCR\r\n" "$3\r\nkey\r\n",
                    ":2\r\n"
                },
                {
                    "*2\r\n" "$4\r\nINCR\r\n" "$4\r\nkey1\r\n",
                    "-ERR Some error\r\n"
                }
            });
        REQUIRE(client->Increment("key") == 1);
        REQUIRE(client->Increment("key") == 2);
        REQUIRE_THROWS_AS(client->Increment("key1"), RedisError);

        server->Finish();
    }

    SECTION("Decrement") {
        auto [client, server] = CreateClientAndServer(
            {
                {
                    "*2\r\n" "$4\r\nDECR\r\n" "$3\r\nkey\r\n",
                    ":-1\r\n"
                },
                {
                    "*2\r\n" "$4\r\nDECR\r\n" "$3\r\nkey\r\n",
                    ":-2\r\n"
                },
                {
                    "*2\r\n" "$4\r\nDECR\r\n" "$4\r\nkey1\r\n",
                    "-ERR Some error\r\n"
                }
            });
        REQUIRE(client->Decrement("key") == -1);
        REQUIRE(client->Decrement("key") == -2);
        REQUIRE_THROWS_AS(client->Decrement("key1"), RedisError);

        server->Finish();
    }
}