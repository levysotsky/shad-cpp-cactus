add_library(redis
  resp_reader.cpp
  error.cpp
  client.cpp
  simple_storage.cpp
  storage.cpp
  resp_writer.cpp
  server.cpp)

target_link_libraries(redis
  cactus)

add_catch(test_redis_resp_reader
  tests/resp_reader_test.cpp)

add_catch(test_redis_resp_writer
  tests/resp_writer_test.cpp)

add_catch(test_redis_client
  tests/client_test.cpp)

add_catch(test_redis_server
  tests/server_test.cpp)

target_link_libraries(test_redis_resp_reader
  redis)

target_link_libraries(test_redis_resp_writer
  redis)

target_link_libraries(test_redis_client
  redis)

target_link_libraries(test_redis_server
  redis)

add_benchmark(benchmark_redis
  benchmarks/resp_reader.cpp)

target_link_libraries(benchmark_redis
  redis)

add_executable(redis-server
  main.cpp)

target_link_libraries(redis-server redis)