#include <cactus/test.h>

#include "portscan.h"

const folly::SocketAddress kLocalhost("127.0.0.1", 0);
constexpr int startPort = 25000;
constexpr int endPort = 26000;

FIBER_TEST_CASE("Scanning closed ports") {
    auto ports = ScanPorts(kLocalhost, startPort, endPort, std::chrono::seconds(1));

    REQUIRE(ports.size() == 1000);
    for (int i = 0; i < (int)ports.size(); i++) {
        REQUIRE(ports[i].first == startPort + i);
        REQUIRE(ports[i].second == PortState::Closed);
    }
}

FIBER_TEST_CASE("Find open ports") {
    std::vector<std::unique_ptr<cactus::IListener>> lsns;
    for (int port : {startPort+1, startPort+600}) {
        auto addr = kLocalhost;
        addr.setPort(port);
        lsns.emplace_back(cactus::ListenTCP(addr));
    }

    auto ports = ScanPorts(kLocalhost, startPort, endPort, std::chrono::seconds(1));

    REQUIRE(ports.size() == 1000);
    for (int i = 0; i < (int)ports.size(); i++) {
        REQUIRE(ports[i].first == startPort + i);

        if (ports[i].first != startPort+1 && ports[i].first != startPort+600) {
            REQUIRE(ports[i].second == PortState::Closed);
        } else {
            REQUIRE(ports[i].second == PortState::Open);
        }
    }
}