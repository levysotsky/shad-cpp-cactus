#include <cactus/cactus.h>

enum class PortState {
    Open, Closed, Timeout
};

std::vector<std::pair<int, PortState>> ScanPorts(const folly::SocketAddress& remote, int start, int end, cactus::Duration timeout);