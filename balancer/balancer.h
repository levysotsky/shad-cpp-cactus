#include <cactus/cactus.h>

class IBalancer {
public:
    virtual ~IBalancer() = default;
    virtual void SetBackends(const std::vector<folly::SocketAddress>& peers) = 0;
    virtual void Run() = 0;
};

std::unique_ptr<IBalancer> CreateBalancer(const folly::SocketAddress& address);